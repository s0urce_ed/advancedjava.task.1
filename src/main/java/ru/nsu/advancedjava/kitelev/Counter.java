package ru.nsu.advancedjava.kitelev;

public class Counter implements Comparable<Counter> {
    private int count = 0;

    public int value() {
        return count;
    }

    public void inc() {
        count++;
    }

    @Override
    public int compareTo(Counter o) {
        return this.value() - o.value();
    }
}
