package ru.nsu.advancedjava.kitelev;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;

public class MainClass {

    private static final Logger log = LoggerFactory.getLogger(MainClass.class);

    public static void main(String[] args) {
        try {
            FileInputStream fileIS = new FileInputStream(args[0]);
            BZip2CompressorInputStream bzip2IS = new BZip2CompressorInputStream(fileIS);
            Statistics statistics = XmlLoader.load(bzip2IS);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
