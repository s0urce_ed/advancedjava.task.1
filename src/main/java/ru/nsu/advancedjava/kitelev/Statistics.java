package ru.nsu.advancedjava.kitelev;

import java.util.Map;

public class Statistics {

    private final Map<String, Integer> userStatistics;
    private final Map<String, Integer> keyStatistics;

    public Statistics(Map<String, Integer> userStatistics, Map<String, Integer> keyStatistics) {
        this.userStatistics = userStatistics;
        this.keyStatistics = keyStatistics;
    }

    public Map<String, Integer> getUserStatistics() {
        return userStatistics;
    }

    public Map<String, Integer> getKeyStatistics() {
        return keyStatistics;
    }
}
