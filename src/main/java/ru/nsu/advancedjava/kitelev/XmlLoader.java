package ru.nsu.advancedjava.kitelev;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Stream;

public class XmlLoader {

    private static final Logger log = LoggerFactory.getLogger(XmlLoader.class);

    private static final QName NODE_ELEMENT_NAME = new QName("node");
    private static final QName USER_ATTR_NAME = new QName("user");
    private static final QName TAG_ELEMENT_NAME = new QName("tag");
    private static final QName KEY_ATTR_NAME = new QName("k");

    public static Statistics load(InputStream inputStream) {
        Map<String, Counter> user2count = new HashMap<>();
        Map<String, Counter> key2count = new HashMap<>();

        try {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = inputFactory.createXMLEventReader(inputStream);
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartDocument()) {
                    log.debug("Parsing started");
                }

                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    if (startElement.getName().equals(NODE_ELEMENT_NAME)) {
                        Attribute userAttr = startElement.getAttributeByName(USER_ATTR_NAME);
                        String userValue = userAttr.getValue();

                        if (!user2count.containsKey(userValue)) {
                            user2count.put(userValue, new Counter());
                        }
                        user2count.get(userValue).inc();
                    }

                    if (startElement.getName().equals(TAG_ELEMENT_NAME)) {
                        Attribute keyAttr = startElement.getAttributeByName(KEY_ATTR_NAME);
                        String keyValue = keyAttr.getValue();

                        if (!key2count.containsKey(keyValue)) {
                            key2count.put(keyValue, new Counter());
                        }
                        key2count.get(keyValue).inc();
                    }
                }

                if (event.isEndDocument()) {
                    log.debug("Parsing ended");
                }
            }
        } catch (XMLStreamException e) {
            log.error(e.getMessage());
        }

        Map<String, Integer> userStatistics = sortByValue(user2count);
        Map<String, Integer> keyStatistics = sortByValue(key2count);
        return new Statistics(userStatistics, keyStatistics);
    }

    private static Map<String, Integer> sortByValue(Map<String, Counter> map) {
        Map<String, Integer> result = new LinkedHashMap<>();
        Stream<Map.Entry<String, Counter>> st = map.entrySet().stream();

        st.sorted(Collections.reverseOrder(Comparator.comparing(Map.Entry::getValue)))
                .forEachOrdered(e -> result.put(e.getKey(), e.getValue().value()));

        return result;
    }
}
